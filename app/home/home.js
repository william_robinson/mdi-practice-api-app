'use strict';
(function() {
    angular
        .module('practiceApiApp.home', ['ngRoute'])
        .config(['$routeProvider', function ($routeProvider) {
            $routeProvider.when('/home', {
                templateUrl: 'home/home.html',
                controller: 'HomeCtrl as home'
            });
        }])
        .controller('HomeCtrl', HomeController);

    HomeController.$inject = ["PingSvc"];
    function HomeController(PingSvc) {
        var ctrl = this;
        this.Ping = Ping;

        function Ping() {
            ctrl.pingStatus = "Sending ping...";
            PingSvc.Public().then(function (result) {
                ctrl.pingStatus = result;
            });
        }
    }
})();