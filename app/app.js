'use strict';

// Declare app level module which depends on views, and components
angular.module('practiceApiApp', [
    'ngRoute',
    'practiceApiApp.home',
    'practiceApiApp.pings',
    'practiceApiApp.version',
    'practiceApiApp.auth',
    'practiceApiApp.practiceAPI',
    "practiceApiApp.patient"
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
    $locationProvider.html5Mode(true).hashPrefix('!');
    $routeProvider
        .otherwise({redirectTo: '/home'});
}]);
