(function () {
    angular
        .module("practiceApiApp.practiceAPI", [])
        .constant("PracticeApiConfig", {
            UrlBase: "http://localhost:63389/v1/api/"
        });
})();