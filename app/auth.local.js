
angular.module('practiceApiApp.auth', ['AdalAngular'])
    .config(["adalAuthenticationServiceProvider", "$httpProvider",
        function(adalProvider, $httpProvider){
        const endpoints = {
            "http://localhost:63389/v1/api/ping/secure": "https://mdidev.onmicrosoft.com/dbb380e9-5cce-42cb-87be-8920d2f0541a",
            "http://localhost:63389/v1/api/ping/patient": "https://mdidev.onmicrosoft.com/dbb380e9-5cce-42cb-87be-8920d2f0541a",
            "http://localhost:63389/v1/api/ping/practice": "https://mdidev.onmicrosoft.com/dbb380e9-5cce-42cb-87be-8920d2f0541a",
            "http://localhost:63389/v1/api/encounters": "https://mdidev.onmicrosoft.com/dbb380e9-5cce-42cb-87be-8920d2f0541a",
            "http://localhost:63389/v1/api/patients" : "https://mdidev.onmicrosoft.com/dbb380e9-5cce-42cb-87be-8920d2f0541a"
        };
        const anonymousEndpoints = [
            //"https://mdipracticeapi.azurewebsites.net/v1/api/ping"
        ];

        adalProvider.init(
            {
                tenant: "mdidev.onmicrosoft.com",
                clientId: "c6eef4c1-62a2-4f3f-88a6-8679639da10a",
                endpoints: endpoints,
                anonymousEndpoints: anonymousEndpoints,
                redirectUri: "http://localhost:8080/home",
                postLogoutRedirectUri: "http://localhost:8080/home"
            },
            $httpProvider
        );
    }]);
