
angular.module('practiceApiApp.auth', ['AdalAngular'])
    .config(["adalAuthenticationServiceProvider", "$httpProvider",
        function(adalProvider, $httpProvider){
        const endpoints = {
            "http://localhost:63389/v1/api/ping/secure": "https://nextechapibeta.onmicrosoft.com/5aa57a04-ce0d-4c83-aab1-66d4030eb973",
            "http://localhost:63389/v1/api/ping/patient": "https://nextechapibeta.onmicrosoft.com/5aa57a04-ce0d-4c83-aab1-66d4030eb973",
            "http://localhost:63389/v1/api/ping/practice": "https://nextechapibeta.onmicrosoft.com/5aa57a04-ce0d-4c83-aab1-66d4030eb973",
            "http://localhost:63389/v1/api/practices": "https://nextechapibeta.onmicrosoft.com/5aa57a04-ce0d-4c83-aab1-66d4030eb973",
            "http://localhost:63389/v1/api/encounters": "https://nextechapibeta.onmicrosoft.com/5aa57a04-ce0d-4c83-aab1-66d4030eb973",
            "http://localhost:63389/v1/api/patients" : "https://nextechapibeta.onmicrosoft.com/5aa57a04-ce0d-4c83-aab1-66d4030eb973"
        };
        const anonymousEndpoints = [
            //"https://mdipracticeapi.azurewebsites.net/v1/api/ping"
        ];

        adalProvider.init(
            {
                tenant: "nextechapibeta.onmicrosoft.com",
                clientId: "21a6335d-89da-4e56-ac0b-061d697587d4",
                endpoints: endpoints,
                anonymousEndpoints: anonymousEndpoints,
                redirectUri: "http://localhost:8080/home",
                postLogoutRedirectUri: "http://localhost:8080/home"
            },
            $httpProvider
        );
    }]);
