angular
    .module("practiceApiApp.practiceAPI")
    .factory("PingSvc", PingService);

PingService.$inject = ["$http", "PracticeApiConfig"];
function PingService($http, apiConfig){
    return {
        Public: ping,
        Secure: pingSecure,
        Patient: pingPatient,
        Practice: pingPractice,
        GetClaims: getClaims
    }

    function ping(){
        return $http.get(apiConfig.UrlBase + "ping")
            .then(successResult, failureResult);
    }

    function pingSecure(){
        return $http.get(apiConfig.UrlBase + "ping/secure")
            .then(successResult, failureResult);
    }

    function pingPatient(){
        return $http.get(apiConfig.UrlBase + "ping/patient")
            .then(successResult, failureResult);
    }

    function pingPractice(){
        return $http.get(apiConfig.UrlBase + "ping/practice")
            .then(successResult, failureResult);
    }

    function getClaims(){
        return $http.get(apiConfig.UrlBase + "ping/claims")
            .then(function(response){
                return response.data;
            }, failureResult);
    }

    function successResult(response){
        return "Success: " + response.statusText + " - " + response.data;
    }

    function failureResult(response){
        if (response.status === 401){
            return "Error: " + "You are not authorized to access to API.";
        }
        else if (response.status === 403){
            return "Error: " + "You do no have sufficient rights to access this resource."
        }
        return "Error: " + (response.statusText ? (response.status ? response.status : response.statusText) : "Unknown error");
    }
}
