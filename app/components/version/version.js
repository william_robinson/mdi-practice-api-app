'use strict';

angular.module('practiceApiApp.version', [
  'practiceApiApp.version.interpolate-filter',
  'practiceApiApp.version.version-directive'
])

.value('version', '0.4');
