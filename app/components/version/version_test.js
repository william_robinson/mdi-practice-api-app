'use strict';

describe('practiceApiApp.version module', function() {
  beforeEach(module('practiceApiApp.version'));

  describe('version service', function() {
    it('should return current version', inject(function(version) {
      expect(version).toEqual('0.1');
    }));
  });
});
