(function () {
    angular
        .module("practiceApiApp.auth")
        .controller("AuthPanelDirectiveCtrl", AuthPanelDirectiveController)
        .directive("authPanel", AuthPanelDirective);

    function AuthPanelDirective(){
        return {
            restrict: "E",
            scope: {},
            template: `
                    <span>{{ Auth.loginStatus }}</span>
                    <span ng-if="!Auth.IsLoggedIn()">
                        <a href="" ng-click="Auth.AzureLogin()">Azure Login</a>
                    </span>
                    <span ng-if="Auth.IsLoggedIn()">
                        <a href="" ng-click="Auth.Logout()">Logout</a>
                    </span>
                `,
            controller: "AuthPanelDirectiveCtrl as Auth"
        }
    }

    AuthPanelDirectiveController.$inject = ["AuthSvc"];
    function AuthPanelDirectiveController(AuthSvc){
        var vm = this;
        vm.Logout = Logout;
        vm.IsLoggedIn = IsLoggedIn;
        vm.AzureLogin = AzureLogin;

        function IsLoggedIn(){
            return AuthSvc.IsAuthenticated();
        }

        function AzureLogin(){
            AuthSvc.AzureLogin();
        }

        function Logout(){
            AuthSvc.Logout();
        }
    }
})();