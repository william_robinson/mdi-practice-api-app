(function () {
    angular
        .module('practiceApiApp.auth')
        .factory('AuthSvc', AuthService);

    AuthService.$inject = ["$window", "AuthConfig", "authManager", "jwtHelper"];
    function AuthService($window, AuthConfig, authManager, jwtHelper){
        ReadLoginQuery();
        var accessToken = "";
        var idToken = "";
        var status = "";

        return {
            IsAuthenticated: IsAuthenticated,
            AzureLogin: AzureLogin,
            Logout: Logout,
            IsAuthenticated: IsAuthenticated,
            HasRole: HasRole,
            GetPracticeAccess: GetPracticeAccess,
            GetStatusMessage: GetStatusMessage,
            GetToken: GetToken
        }

        function GetToken(){
            var rawToken = authManager.getToken();
            if(!rawToken){
                return {};
            }
            return jwtHelper.decodeToken(rawToken);
        }

        function HasRole(role){
            if(!authManager.isAuthenticated()){
                return false;
            }
            var token = GetToken();
            if (!(token && token.roles && (token.roles instanceof Array))){
                return false;
            }
            return token.roles.find(function(r){ return r == role });
        }

        function GetPracticeAccess(){
            var token = GetToken();
            if(!token){
                return [];
            }
            return token.practices || [];
        }

        function IsAuthenticated(){
            return authManager.isAuthenticated();
        }

        function AzureLogin(){
            localStorage.removeItem("access_token");
            localStorage.removeItem("id_token");
            $window.location = AuthConfig.authorizeUrl;
        }

        function Logout(){
            localStorage.removeItem("access_token");
            localStorage.removeItem("id_token");
            accessToken = "";
            idToken = "";
            status = "Logged out.";
            $window.location = $window.location.origin;
        }

        function GetStatusMessage(){
            return status;
        }

        function ReadLoginQuery(){
            var query = localStorage.getItem("login_query");
            if(query){
                localStorage.removeItem("login_query");

                var accessTokenMatches = query.match(/access_token=([^&]*)/);
                var idTokenMatches = query.match(/id_token([^&]*)/);
                var errorMatches = query.match(/error=([^&]*)/);
                var errMsgMatches = query.match(/error_description=([^&]*)/);

                if(accessTokenMatches) {
                    accessToken = accessTokenMatches[1];
                    localStorage.setItem("access_token", accessToken);
                    idToken = idTokenMatches[1];
                    localStorage.setItem("id_token", idToken);
                    status = "Logged in";
                } else if(errorMatches){
                    var error_name = errorMatches[1];
                    var error_message = errMsgMatches[1];
                    status = "Auth error: " + error_name + " - " + error_message;
                }
            }
        }
    }
})();