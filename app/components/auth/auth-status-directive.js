(function () {
    angular
        .module("practiceApiApp.auth")
        .controller("AuthStatusDirectiveCtrl", AuthStatusDirectiveController)
        .directive("authStatus", AuthStatusDirective);

    function AuthStatusDirective(){
        return {
            restrict: "E",
            scope: {},
            template: `
                   <p ng-if="AuthStatus.loginStatus">
                        {{ AuthStatus.loginStatus }}
                   </p>
                `,
            controller: "AuthStatusDirectiveCtrl as AuthStatus"
        }
    }

    AuthStatusDirectiveController.$inject = ["AuthSvc"];
    function AuthStatusDirectiveController(AuthSvc){
        var vm = this;
        vm.loginStatus = AuthSvc.GetStatusMessage();
    }
})();