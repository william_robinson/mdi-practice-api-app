(function () {
    angular
        .module("practiceApiApp.patient")
        .factory("PatientSvc", PatientService);

    PatientService.$inject = ["$rootScope", "adalAuthenticationService", "$http", "PracticeApiConfig"];
    function PatientService($rootScope, adalProvider, $http, apiConfig){
        var user = $rootScope.userInfo;

        var currentPatient = {
            PracticeShortName: "",
            PatientId: "",
            ChartNumber: "",
            UserName: user.profile ? user.profile.name : "",
            IsAuthenticated: user.isAuthenticated,
        }

        return {
            RequestCCD: requestCCD,
            RequestEncounters: requestEncounters,
            Patient: currentPatient,
            Search: search,
            Logout: logout,
            RequestCategories: requestCategories
        }

        function search(searchTerm){
            return $http.get(apiConfig.UrlBase + "patients/search?term=" + searchTerm)
                .then(successResult, failureResult);
        }

        function requestCCD(encounterId){
            return $http.get(apiConfig.UrlBase + "encounters/" + encounterId + "/ccd")
                .then(successResult, failureResult);
        }

        function requestEncounters(patientId, startDate, endDate, section){
            return $http.get(apiConfig.UrlBase + "patients/" + patientId
                + "/encounters/search?start=" + (startDate.getMonth() + 1) + "-"
                + startDate.getDate() + "-" + startDate.getFullYear() + "&end="
                + (endDate.getMonth() + 1) + "-"
                + endDate.getDate() + "-" + endDate.getFullYear()
                + (section ? "&section=" + section.toLowerCase() : ""))
                .then(successResult, failureResult);
        }

        function successResult(response){
            return response.data;
        }

        function failureResult(response){
            if (response.status === 401){
                return "You are not authorized to access to API.";
            }
            else if (response.status === 403){
                return "You do no have sufficient rights to access this resource."
            }
            return "Error: " + (response.statusText ? response.statusText : "Unknown error");
        }

        function logout(){
            adalProvider.logOut();
        }

        function requestCategories(){
            return $http.get(apiConfig.UrlBase + "encounters/categories")
                .then(successResult, failureResult);
        }
    }
})();