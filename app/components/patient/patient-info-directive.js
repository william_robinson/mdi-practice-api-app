(function () {
    angular
        .module("practiceApiApp.patient")
        .controller("PatientInfoCtrl", PatientInfoController)
        .directive("patientInfo", PatientInfoDirective);

    function PatientInfoDirective(){
        return {
            restrict: "E",
            controller: "PatientInfoCtrl as patientInfo",
            template:`
                <span ng-if="patientInfo.Patient.IsAuthenticated">
                    <span>{{ patientInfo.Patient.UserName }}</span>
                    <a href="" ng-click="patientInfo.Logout()">Logout</a>
                </span>
            `
        }
    }

    PatientInfoController.$inject = ["PatientSvc"];
    function PatientInfoController(PatientSvc){
        let ctrl = this;
        ctrl.Patient = PatientSvc.Patient;
        ctrl.Logout = Logout;

        function Logout(){
            PatientSvc.Logout();
        }
    }
})();