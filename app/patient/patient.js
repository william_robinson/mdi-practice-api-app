(function () {
    angular
        .module("practiceApiApp.patient", ["AdalAngular"])
        .controller("PatientCtrl", PatientController)
        .config(["$routeProvider", function($routeProvider){
            $routeProvider.when("/patient", {
                templateUrl: 'patient/patient.html',
                controller: 'PatientCtrl as patientCtrl',
                requireADLogin: true
            });
        }]);

    PatientController.$inject = ["PatientSvc"];
    function PatientController(PatientSvc){
        let ctrl = this;
        ctrl.RequestCCD = RequestCCD;
        ctrl.RequestEncounterSummaries = RequestEncounterSummaries;
        ctrl.Search = Search;
        ctrl.DisplayName = DisplayName;

        ctrl.EncounterList = [];
        ctrl.Encounter = null;
        ctrl.Patient = null;
        ctrl.PatientList = null;
        ctrl.SearchStartDate = null;
        ctrl.SearchEndDate = null;
        ctrl.IsRequestingCCD = false;
        ctrl.IsRequestingDates = false;
        ctrl.PracticeId = 1001;
        ctrl.CCD = null;
        ctrl.Category = null;

        function DisplayName(patient){
            return patient.lastName + ', ' + patient.firstName + ' (' + patient.chartNumber + ')';
        }

        function Search(){
            ctrl.Patient = null;
            ctrl.PatientList = null;
            ctrl.EncounterList = [];
            ctrl.Encounter = null;
            ctrl.CCD = null;

            ctrl.IsSearchingPatients = true;
            PatientSvc.Search(ctrl.SearchTerm).then(function(result){
                ctrl.PatientList = result;
                ctrl.IsSearchingPatients = false;
            }, function(){
                ctrl.IsSearchingPatients = false;
            })
        }

        function RequestCCD(){
            ctrl.CCD = null;
            ctrl.IsRequestingCCD = true;
            PatientSvc.RequestCCD(ctrl.Encounter.encounterId).then(function(result) {
                ctrl.IsRequestingCCD = false;
                ctrl.CCD = result;
            }, function(){
                ctrl.IsRequestingCCD = false;
                ctrl.CCD = null;
            });
        }

        function RequestEncounterSummaries(){
            ctrl.EncounterList = [];
            ctrl.Encounter = null;
            ctrl.IsRequestingDates = true;
            ctrl.CCD = null;
            PatientSvc.RequestEncounters(ctrl.Patient.patientId, ctrl.SearchStartDate, ctrl.SearchEndDate, ctrl.Category)
                .then(function(result){
                    ctrl.IsRequestingDates = false;
                    ctrl.EncounterList = result;
                }, function() {
                    ctrl.IsRequestingDates = false;
                    ctrl.EncounterList = [];
                }
            );
        }

        function RequestDataCategories(){
            PatientSvc.RequestCategories().then(function(result){
                ctrl.CategoryList = result;
            });
        }

        RequestDataCategories();
    }
})();