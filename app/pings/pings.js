'use strict';
(function(){
    angular
        .module('practiceApiApp.pings', ['ngRoute'])
        .config(['$routeProvider', function ($routeProvider) {
            $routeProvider.when('/pings', {
                templateUrl: 'pings/pings.html',
                controller: 'PingsCtrl as pings',
                requireADLogin: true
            });
        }])
        .controller('PingsCtrl', PingsController);

    PingsController.$inject = ["PingSvc"];
    function PingsController(PingSvc){
        var ctrl = this;
        ctrl.PingSecure = PingSecure;
        ctrl.PingPatient = PingPatient;
        ctrl.PingPractice = PingPractice;
        ctrl.Log = "Request a ping.\n";

        function PingPatient(){
            ctrl.Log += "Requesting Patient ping...\n";
            PingSvc.Patient().then(LogResponse);
        }

        function PingPractice(){
            ctrl.Log += "Requesting Practice ping...\n";
            PingSvc.Practice().then(LogResponse);
        }

        function PingSecure(){
            ctrl.Log += "Requesting Secure ping...\n";
            PingSvc.Secure().then(LogResponse);
        }

        function LogResponse(response){
            ctrl.Log += (response + "\n");
        }
    }

})();
