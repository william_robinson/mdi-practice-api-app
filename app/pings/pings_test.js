'use strict';
(function(){
    describe('practiceApiApp.pings module', function() {

        beforeEach(module('practiceApiApp.pings'));

        describe('pings controller', function(){

            it('should ....', inject(function($controller) {
                //spec body
                var pingsCtrl = $controller('PingsCtrl');
                expect(pingsCtrl).toBeDefined();
            }));
        });
    });
})();
